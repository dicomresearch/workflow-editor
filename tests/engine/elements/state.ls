require! transitionElement

chai.should!

describe 'State element', (_) !->
    it 'must have type wf.State', !->
        transition = new joint.shapes.wf.State!

        transition.should.be.a \object
        transition.attributes.should.have.any.keys \type
        transition.attributes.type.should.be.equal \wf.State
