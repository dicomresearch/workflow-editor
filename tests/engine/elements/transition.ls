require! transitionElement

chai.should!

describe 'Transition element', (_) !->
    it 'must have type wf.Transition', !->
        transition = new joint.shapes.wf.Transition!

        transition.should.be.a \object
        transition.attributes.should.have.any.keys \type
        transition.attributes.type.should.be.equal \wf.Transition
