require! {
    engine
    mockWF
}

chai.should!
engineInstance = undefined

beforeEach !->
    engineInstance := engine.create!

describe 'Engine', (_) !->
    it 'create() method return object with Graph and Paper props', !->
        engineInstance.should.be.a \object
        engineInstance.should.have.any.keys 'Graph', 'Paper'

    it 'createLayout() method return object with Layout prop', !->
        layout = engineInstance.createLayout!

        layout.should.be.a \object
        layout.should.have.any.keys 'Layout'

    it 'generateUMLs() method return array', !->
        umls = engine.generateUMLs mockWF.states

        umls.should.be.a \array

    it 'generateRelations() method return array', !->
        relations = engine.generateRelations mockWF.transitions

        relations.should.be.a \array

    it 'createUML() method return object', !->
        uml = engine.createUML name: 'test'

        uml.should.be.a \object

    it 'createRelation() method return object', !->
        relation = engine.createRelation name: 'test'

        relation.should.be.a \object
