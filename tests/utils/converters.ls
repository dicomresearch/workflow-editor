require! {
    'converters': Converter
    mockWF
}

chai.should!

describe 'Converter', (_) !->
    it 'rulesToApp() method return array objects then must have "name" property', !->
        data = mockWF.transitions[0].rules
        afterConvertData = Converter.rulesToApp data

        afterConvertData.should.be.a \array
        afterConvertData[0].should.be.a \object
        afterConvertData[0].should.have.any.keys \name

    it 'actionsToApp() method return array objects then must have "name" property', !->
        data = mockWF.transitions[0].client.actions
        afterConvertData = Converter.actionsToApp data

        afterConvertData.should.be.a \array
        afterConvertData[0].should.be.a \object
        afterConvertData[0].should.have.any.keys \name

    it 'propertiesToApp() method return array objects then must have "name" property', !->
        data = mockWF.states.described.properties
        console.log data
        afterConvertData = Converter.propertiesToApp data

        afterConvertData.should.be.a \array
        afterConvertData[0].should.be.a \object
        afterConvertData[0].should.have.any.keys \name
