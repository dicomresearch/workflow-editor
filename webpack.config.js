var webpack = require('webpack');

module.exports = {
    entry: ['./src/index.ls'],
    output: {
        path: './app',
        filename: 'bundle.js'
    },
    plugins: [
        new webpack.ProvidePlugin({
            App: 'app.ls',
            fn: 'functions.ls',
            $: 'jquery',
            jQuery: 'jquery',
            joint: 'jointjs',
            _: 'lodash',
            Backbone: 'backbone',
            Marionette: 'marionette',
            Engine: 'engine'
        })
    ],
    resolve: {
        modulesDirectories: ['app', 'node_modules', 'engine', 'utils'],
        alias: {
            'marionette': __dirname + '/node_modules/backbone.marionette/lib/backbone.marionette.js',
            'jquery': __dirname + '/node_modules/jquery/dist/jquery.js',
            'jointCSS': __dirname + '/node_modules/jointjs/joint.css',
            'engine': __dirname + '/src/scripts/engine/engine.ls'
        }
    },
    module: {
        loaders: [
            { test: /\.ls$/, loader: 'livescript-loader' },
            { test: /\.jade$/, loader: 'jade-loader' },
            { test: /\.css$/, loader: 'style-loader!css-loader' },
            { test: /\.styl$/, loader: 'style-loader!css-loader!stylus-loader' },
            { test: /\.(eot|woff2|woff|ttf)$/i, loader: 'file-loader' },
            { test: /\.(jpe?g|png|gif|svg)$/i, loader: 'file-loader' }
        ]
    }
};
