/*require! {
    'components/editWindow/WFItemCollection.ls': Collection
    'components/editWindow/WFItemModel.ls': Model
}

function toCollection array
    arrayModels = _.map array, (obj) ->
        obj.children = toCollection obj.children
        new Model obj

    new Collection arrayModels
*/

/*Конвертация стандартного массива действий в ВФ*/
function rulesToWF (data) then _.map data, (rules) -> {"#{rules.name}": _.map rules.children, 'name'}

/*Конвертация стандартного массива действий в ВФ*/
function actionsToWF data
    wfObj = {}

    _.forEach data, (action) -> wfObj[action.name] = _.map action.children, 'name'

    wfObj

/*Конвертация стандартного массива свойств в ВФ*/
function propertiesToWF data
    wfObj = {}

    function deciderChildren child
        switch child.value |> _.isUndefined
        | true => return child.name
        | false =>
            complexProp = {}
            complexProp[child.name] = child.value
            return complexProp

    function mapChildren children then _.map children, deciderChildren
    function recursionInner children then {properties: children |> propertiesToWF}

    act = (data, fn) --> data |> fn
    ref = (target, path,  subject) ~~> _.set target, path, subject
    refToWFObj = ref wfObj

    _.forEach data, (prop) !->
        actOnChildren = prop.children |> act
        refPropName = refToWFObj prop.name

        if prop.inner
        then recursionInner |> actOnChildren |> refPropName
        else mapChildren |> actOnChildren |> refPropName

    wfObj

/*Конвертация свойств из ВФ в стандарт*/
function deciderForProps (rules, name)
    if _.isArray rules
    then
        rules = _.map rules, (obj, index) ->
            if _.isObject obj
            then
                newObj = {}

                _.forEach obj, (value, key) !->
                    newObj :=
                        name: key
                        children: []

                    if _.isArray value
                    then newObj.children = value
                    else newObj.value = value

                return newObj
            else
                if _.isString obj
                then
                    return {
                        name: obj
                        children: []
                    }

        return {
            name: name
            children: rules
        }

    else
        if rules.properties
        then
            return {
                name: name
                inner: true
                children: proprsConverter rules.properties
            }

function proprsConverter (props) then _.map props, deciderForProps

/*Конвертация экшнов из ВФ в стандарт*/
function deciderForActions (action, name)
    if _.isArray action
    then
        return {
            name: name
            children: actionsConverter action
        }

    else
        if _.isString action
        then
            return {
                name: action
                children: []
            }

function actionsConverter (actions) then _.map actions, deciderForActions

/*Конвертация правил из ВФ в стандарт*/
function deciderforRules (rule, index)
    if _.isObject rule
    then
        newRule = {}

        _.forEach rule, (value, key) !->
            newRule :=
                name: key
                children: rulesConverter value

        return newRule
    else
        if _.isString rule
        then
            return {
                name: rule
                children: []
            }

function rulesConverter (rules) then _.map rules, deciderforRules

export
    propertiesToWF: (properties) -> propertiesToWF properties
    propertiesToApp: (props) -> proprsConverter props
    actionsToWF: (actions) -> actionsToWF actions
    actionsToApp: (actions) -> actionsConverter actions
    rulesToWF: (rules) -> rulesToWF rules
    rulesToApp: (rules) -> rulesConverter rules
    /*toCollection: (arr) -> toCollection arr*/
