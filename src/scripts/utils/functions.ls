isEqual = (val, otherVal) --> _.isEqual val, otherVal
isExist = --> !_.every &, _.isUndefined

export
    isExist
    isEqual
