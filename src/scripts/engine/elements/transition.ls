joint.shapes.wf = joint.shapes.wf || {}

function rerender context
    view = Engine.Paper.findViewByModel context
    view?render!

function idUpdate element, value, target
    newId = Engine.getUMLIdByNamePublic element.collection.models, value
    element.get(target).id = newId

joint.shapes.wf.Transition = joint.dia.Link.extend do
    defaults: joint.util.deepSupplement do
        type: 'wf.Transition'
        name: ''

        attrs:
            '.connection': stroke: '#025C55', 'stroke-width': 3
            '.marker-target': stroke: '#025C55', fill: '#025C55', d: 'M 15 0 L 0 5 L 15 15 z'

        labels: [
            *   position: 0.5
                attrs:
                    '.': {}
                    text: fill: 'white', 'font-family': 'Arial', 'font-size': 18, 'font-weight': 'bold'
                    rect: stroke: '#0D6AB6', fill: '#0D6AB6', 'stroke-width': 20, rx: 5, ry: 5
        ]

        , joint.dia.Link.prototype.defaults

    initialize: (opts, paper) ->
        @on do
            'change:name': @updateName
            'change:oldState': @updateOldState
            'change:newState': @updateNewState

        @updateName!

        if @attributes.source.id == @attributes.target.id then @attributes.loop = true

        joint.dia.Link.prototype.initialize.apply @, arguments

    updateName: !->
        labels = @get \labels
        labelObj = labels[0].attrs.text
        labelObj.text = @get \name

        @ |> rerender

    updateOldState: (element, newValue) !->
        idUpdate element, newValue, 'source'
        @ |> rerender

    updateNewState: (element, newValue) !->
        idUpdate element, newValue, 'target'
        @ |> rerender
