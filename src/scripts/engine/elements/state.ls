joint.shapes.wf = joint.shapes.wf || {}

joint.shapes.wf.State = joint.shapes.basic.Rect.extend do
    markup: [
        '<g class="rotatable">'
            '<g class="scalable">'
                '<rect class="state"/>'
            '</g>'
            '<text class="state-name"/>'
            '<text class="state-comment"/>'
        '</g>'
    ].join('')

    defaults: joint.util.deepSupplement do
        type: 'wf.State'
        size: width: 400 height: 100

        name: ''
        comment: ''

        attrs:
            '.': {}
            rect: {}
            text: {}

            '.state':
                fill: '#15A99D'
                stroke: '#025C55'
                'stroke-width': 5

            '.state-name':
                'ref': '.state', 'ref-y': 0.25, 'ref-x': 0.5, 'text-anchor': 'middle', 'font-weight': 'bold',
                'fill': 'white', 'font-size': 24, 'font-family': 'Arial'

            '.state-comment':
                'ref': '.state', 'ref-y': 0.55, 'ref-x': 0.5,'text-anchor': 'middle', 'font-weight': 'italic',
                'fill': 'white', 'font-size': 18, 'font-family': 'Arial'

        , joint.shapes.basic.Rect.prototype.defaults

    initialize: !->
        @on do
            'change:name': @updateName
            'change:comment': @updateComment
            , @

        @updateName!
        @updateComment!

        joint.shapes.basic.Rect.prototype.initialize.apply @, arguments

    updateName: !-> @attr '.state-name/text', @get \name
    updateComment: !-> @attr '.state-comment/text', @get \comment
