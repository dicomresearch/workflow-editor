require! {
    'observer.ls'
    'elements/state.ls'
    'elements/transition.ls'
    'converters.ls': Converter
}

# arr: array, separator: string -> string
function arrToString arr, separator then arr.join separator

# props: object, separator: string -> string
function arrObjectsToString props, separator
    resultArr = []
    x = _.forEach props, (obj) ->
            _.forEach obj, (val, key) ->
                resultArr.push "#{key}: #{val}"

    arrToString resultArr, separator

# props: object, separator: string -> [string]
function objToString props, separator
    obj = props.properties

    _.map obj, (val, key) ->
        arrString = arrObjectsToString val, separator
        "#{key}: #{arrString}"

# определяет, какому обработчику отдать пришедшие данные свойств
function propsToString props, separator
    if _.isArray props then return arrToString props, separator
    if _.isObject props then return objToString props, separator

# string, string -> string
function createUMLDataString key, val then "#key: #val"

# [], string -> [string]
function createUMLDataFromStateData val, key
    createUMLDataString do
        key
        propsToString val, ', '

# [] -> [string]
function stateDataToUMLData props then _.map props, createUMLDataFromStateData

# joint.dia.Graph -> [joint.shapes.uml.Class]
function getUMLsFrom graph then graph.attributes.cells.models

# [joint.shapes.uml.Class] -> number
function getUMLIdByName umls, name
    _.result do
        _.result do
            _.findWhere do
                umls
                attributes: name: name
            \attributes
        \id

# change graph
function setUMLsTo graph, umls then _.each umls, (uml) -> graph.addCell uml

# change graph
function setRelationsTo graph, relations then _.each relations, (relation) -> graph.addCell relation

# {} -> []
function parseWFStates states then _.map states, (val, key) -> _.merge val, name: key

# [] -> []
function unloopTrasitions elements
    _.map elements, (v) ->
        if v.attributes.loop
        then
            id = v.attributes.source.id
            bindElement = _.findWhere elements, {id}
            bindElementPosition = bindElement.get \position

            unloopVertices =
                * x: bindElementPosition.x
                  y: bindElementPosition.y - 100

                * x: bindElementPosition.x + 400
                  y: bindElementPosition.y - 100

            v.set \vertices , unloopVertices

        return v

export
    # String -> Engine
    create: (element = '#paper') ->
        # joint.dia.Graph
        @Graph = new joint.dia.Graph

        # joint.dia.Paper
        @createPaper element, @Graph

        @

    /*
     * element: string
     * graph: joint.dia.Graph
     *
     * change Engine
     */
    createPaper: (element, graph) !->
        @Paper = new joint.dia.Paper do
                    el: element
                    width: 6000
                    height: 3000
                    model: graph
                    gridSize: 10

        # обработчик даблклика по элементу схемы
        @Paper.on 'cell:pointerdblclick', (element) !->
            if element.model.isLink!
            then observer.trigger \transition:dblclick , element
            else observer.trigger \state:dblclick , element

        # обработчик ховера элемента схемы
        @Paper.on 'cell:mouseover', (element) !->
            if element.model.isLink!
            then observer.trigger \transition:hover , element
            else observer.trigger \state:hover , element

    # [] -> []
    generateUMLs: (states) -> _.map states, (state) ~> @createUML state

    # [] -> []
    generateRelations: (transitions) -> _.map transitions, (transition) ~> @createRelation transition

    # {} -> Engine
    generate: (wf) ->
        setUMLsTo @Graph, @generateUMLs parseWFStates wf.states
        setRelationsTo @Graph, @generateRelations wf.transitions

        @createLayout!

        unloopTrasitions @Paper.model.attributes.cells.models

        @

    # {} -> Engine
    createLayout: ->
        @Layout = joint.layout.DirectedGraph.layout @Graph, do
            edgeSep: 300
            rankSep: 200
            rankDir: \LR

        @

    # { name: String, properties: Array, actions: Array } -> joint.shapes.uml.Class
    createUML: ->
        state =
            name: it.name
            comment: it._comment
            transition: false

        props = Converter.propertiesToApp it.properties
        actions = Converter.actionsToApp it.actions

        if not _.isEmpty props then state.properties = props
        if not _.isEmpty actions then state.actions = actions

        new joint.shapes.wf.State state

    # sourceId: number, targetId: number, name: string -> joint.shapes.uml.Generalization
    createRelation: (it, fromForm)->
        /*new joint.dia.Link do*/
        trans =
            name: it.actionName || it.name
            comment: it._comment || it.comment
            oldState: it.oldState
            newState: it.newState
            transition: true
            source: id:  getUMLIdByName(getUMLsFrom(@Graph), it.oldState)
            target: id: getUMLIdByName(getUMLsFrom(@Graph), it.newState)

        if fromForm
        then
            rules = it.rules
            actions = it.actions
        else
            rules = Converter.rulesToApp it.rules
            actions = Converter.actionsToApp it.client?actions

        if not _.isEmpty rules then trans.rules = rules
        if not _.isEmpty actions then trans.actions = actions

        new joint.shapes.wf.Transition trans

    /*DEV*/
    getCurrentWF: ->
        stateToWF = (data) ->
            wfData = {}

            if data.name then wfData[data.name] = {}
            if data.comment then wfData[data.name]._comment = data.comment
            if data.properties then wfData[data.name].properties = Converter.propertiesToWF data.properties
            if data.actions then wfData[data.name].actions = Converter.actionsToWF data.actions

            wfData

        transitionToWF = (data) ->
            wfData = {}

            if data.oldState then wfData.oldState = data.oldState
            if data.newState then wfData.newState = data.newState
            if data.name then wfData.actionName = data.name
            if data.comment then wfData._comment = data.comment
            if data.rules then wfData.rules = Converter.rulesToWF data.rules
            if data.actions then wfData.client = {actions: Converter.actionsToWF data.actions}

            wfData

        diaElementsCollection = @Graph.get \cells
        diaElementsArr = diaElementsCollection.toJSON!

        WF = {
            states: {},
            transitions: []
        }

        _.forEach diaElementsArr, (obj) ->
            switch obj.type
            | 'wf.State' => _.merge WF.states, stateToWF obj
            | 'wf.Transition' => WF.transitions.push transitionToWF obj


        WF

    # [joint.shapes.uml.Class] -> number
    getUMLIdByNamePublic = (umls, name) -> getUMLIdByName umls, name
