require! {
    'main/MainView.ls'
    'main/MainModule.ls'
    'main/mainMenu/MainMenu.ls'
    'editor/EditorView.ls'
    'editor/elements/StateModule.ls'
    'editor/elements/TransitionModule.ls'
}

export
    components: {}
    modules: {}
    views: {}

$ '.ui.modal' .modal do
    allowMultiple: true
    closable: false
