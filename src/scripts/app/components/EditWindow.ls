require! {
    \semantic-ui/dist/semantic.css
    \semantic-ui/dist/semantic.js
    \components/editWindow/Tabs.ls
    \./EditWindow.jade : template
}

EditWindow = Marionette.View.extend do
    el: $ \#editWindow
    children: []

    show: ->
        @$el.modal \show
        @

    close: ->
        @$el.modal \hide
        @

    ui:
        removeBtn: '#removeElement'
        saveBtn: '#saveEditWinData'
        close: '.close'

    events:
        'click @ui.removeBtn': 'removeElement'
        'click @ui.saveBtn': 'saveData'
        'click @ui.close': 'close'

    removeElement: ->
        answer = confirm 'Вы уверены, что хотите удалить этот элемент?'

        removeRelations = (data) ->
            if not data.model.get \transition
            then
                cells = data.model.collection.models
                name = data.model.get \name
                nameEqual = fn.isEqual name

                relationArray = _.filter cells, (element) ->
                    oldState = element?get \oldState
                    newState = element?get \newState
                    oldOrNewStatesConditional = (oldState, newState) -> (oldState |> nameEqual) or (newState |> nameEqual)
                    isRelation = oldOrNewStatesConditional oldState, newState

                    isRelation

                _.forEach relationArray, (relation) -> relation.remove!

        if answer
        then
            removeRelations @data
            @data.model.remove!
            @close!

    saveData: (e)->
        e.stopPropagation!

        _.forEach @children, (obj) ~>
            newData = obj.el.triggerMethod \save
            @deciderSave obj.name, newData

        alert 'Данные сохранены!'

    saveMain: (target, data) !-> target.model.set data
    saveProps: (target, data) -> target.model.set \properties , data
    saveActions: (target, data) -> target.model.set \actions , data
    saveRules: (target, data) -> target.model.set \rules , data

    deciderSave: (type, data) ->
        mapper=
            main: @saveMain
            props: @saveProps
            actions: @saveActions
            rules: @saveRules

        mapper[type](@data, data)

    # it: joint.dia.Element
    render: ->
        @data = it
        @$el.html template!

        tabs = new Tabs do
            model: @data?model
            parent: @

        @$el.find \div.content .html tabs.render!el

        $ '.ui.tabular.menu .item' .tab!
        $ '.ui.accordion' .accordion!

        @

    activate: ->
        @show!
        @

export
    class: EditWindow
    create: ->
        @element = new EditWindow
        @

    # it: joint.dia.Element
    activate: ->
        @element.render it
        @element.activate!
        @
