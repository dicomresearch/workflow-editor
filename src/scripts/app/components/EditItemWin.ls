require! {
    './EditItemWin.jade':template
    'components/editWindow/WFItemModel.ls':Model
}

EditItemWin = Marionette.ItemView.extend do
    el: $ \#editItemWin
    template: template

    render: ->
        @data = it
        @$el.html template it?model?toJSON!
        @

    show: ->
        @$el.modal \show
        @

    close: ->
        @$el.modal \hide
        @

    ui:
        saveBtn: '#saveItemData'

    events:
        'click @ui.saveBtn': 'saveData'
        'click .close': 'close'

    saveData: (e) !->
        e.stopPropagation!

        $nameInput = $ '.nameField'
        $valueInput = $ '.valueField'

        newData =
            name: $nameInput.val!
            value: $valueInput.val!

        switch @data.type
        | 'edit' => newData |> @data.model.set
        | 'add' => @data.context.collection.add <| new Model newData

        @close!

    activate: ->
        @show!
        @

export
    class: EditItemWin
    create: ->
        @element = new EditItemWin
        @

    # it: joint.dia.Element
    activate: ->
        @element.render it
        @element.activate!
        @
