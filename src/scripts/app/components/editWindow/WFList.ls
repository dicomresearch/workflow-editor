require! {
    'components/editWindow/WFCompositeItem.ls'
    'components/editWindow/WFItemCollection.ls': Collection
    'components/editWindow/WFItemModel.ls': Model
    './WFList.jade': template
}

module.exports = Marionette.CollectionView.extend do
    className: 'ui propList fluid styled accordion'
    childView: WFCompositeItem

    render: ->
        @$el.html template!
        Marionette.CollectionView.prototype.render.call @, &
        @

    initialize: !-> @collection = new Collection @collection

    onSave: -> @collection.toJSON!

    ui:
        addParent:'.addParent'

    events:
        'click @ui.addParent': \add

    add: (e) !->
        e.stopPropagation!

        App.components.editItemWindow.activate do
            context: @
            type: 'add'
