require! {
    \./MainForm.jade : template
}

module.exports = Marionette.ItemView.extend do
    template: template

    onSave: ->
        $oldState = @$el.find 'input[name="oldState"]'
        $newState = @$el.find 'input[name="newState"]'
        $name = @$el.find 'input[name="name"]'
        $comment = @$el.find 'textarea[name="comment"]'

        newDate =
            oldState: $oldState.val!
            newState: $newState.val!
            name: $name.val!
            comment: $comment.val!

        newDate
