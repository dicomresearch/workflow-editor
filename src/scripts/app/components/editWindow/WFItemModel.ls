require! {
    \components/editWindow/WFItemCollection.ls : Collection
}

Model = Backbone.Model.extend do
    defaults:
        name: 'new'
        children: []

module.exports = Model
