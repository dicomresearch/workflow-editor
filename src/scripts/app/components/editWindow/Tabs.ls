require! {
    \./Tabs.jade : template
    \./MainForm.ls
    \./WFList.ls
}

/*Рендер таба*/
function renderTab mount, name, tab then mount.find ".tab[data-tab='#{name}']" .html tab.render!el

/*Вложить элемент в родителя*/
function setChildToParent children, name, tab
    children.push do
        name: name
        el: tab

/*Создать таб, отрендерить и вложить в родителя*/
function createTab context, data, name, constructor
    tab = new constructor data

    setChildToParent context.options.parent.children, name, tab
    renderTab context.$el, name, tab

/*Создать и отрендерить табы по схеме*/
function createTabs context, schema then _.forEach schema, (obj) -> createTab context, obj.data, obj.name, obj.constructor

module.exports = Marionette.ItemView.extend do
    template: template

    onRender: ->
        @createChildren!
        @

    stateSchema: (model) -> [
        * name: \main
          constructor: MainForm
          data: {@model}

        * name: \props
          constructor: WFList
          data: collection: model.get \properties

        * name: \actions
          constructor: WFList
          data: collection: model.get \actions
    ]

    transitionSchema: (model) -> [
        * name: \main
          constructor: MainForm
          data: {@model}

        * name: \rules
          constructor: WFList
          data: collection: model.get \rules

        * name: \actions
          constructor: WFList
          data: collection: model.get \actions
    ]

    deciderSchema: ->
        switch @model.get \type
        | \wf.State         => @stateSchema @model
        | \wf.Transition   => @transitionSchema @model

    createChildren: ->
        createTabs @, @deciderSchema!
        @
