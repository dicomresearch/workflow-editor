require! {
    './WFItemModel.ls': Model
    'components/editWindow/WFItemCollection.ls': Collection
    'components/editWindow/valueTemplate.jade': valueTemplate
    'components/editWindow/childrenTemplate.jade': childrenTemplate
    'components/editWindow/generalTemplate.jade': generalTemplate
}

WFCompositeItem = Marionette.CompositeView.extend do
    childView: WFCompositeItem
    childViewContainer: '.content>.accordion'

    initialize: !->
        children = ~> @model.get \children
        childrenSetCollection = ~> @model.set \children , @collection

        if _.isArray children!
        then @collection = new Collection children!
        else if _.isObject children! then @collection = children!

        childrenSetCollection!

        /*@collection = new Collection _.cloneDeep @model.get \children*/

    templateDecider: !->
        value = @model.get \value
        children = @model.get \children

        valueIsExist = fn.isExist value
        childrenIsExist = fn.isExist children
        optsIsExist = valueIsExist and childrenIsExist

        if valueIsExist then @template = valueTemplate
        else if childrenIsExist then @template = childrenTemplate
        else @template = generalTemplate

    onBeforeRender: !->
        @templateDecider!

    modelEvents:
        'change': 'modelChanged'

    modelChanged: !-> @render!

    ui:
        addBtn:'.addItem'
        editBtn:'.editItem'
        rmBtn:'.removeItem'

    events:
        'click @ui.rmBtn': \rm
        'click @ui.addBtn': \add
        'click @ui.editBtn': \edit

    edit: (e) !->
        e.stopPropagation!

        App.components.editItemWindow.activate do
            model: @model
            context: @
            type: 'edit'

    add: (e) !->
        e.stopPropagation!

        App.components.editItemWindow.activate do
            context: @
            type: 'add'

    rm: (e) !->
        e.stopPropagation!

        answer = confirm 'Вы уверены, что хотите удалить этот элемент?'

        if answer then @model.destroy!

module.exports = WFCompositeItem
