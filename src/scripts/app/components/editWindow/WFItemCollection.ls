require! {
    \components/editWindow/WFItemModel.ls : Model
}

Model = Backbone.Model.extend do
    defaults:
        name: 'new'
        children: []

module.exports = Backbone.Collection.extend do
    model: Model
