require! {
    'semantic-ui/dist/components/list.css'
    'semantic-ui/dist/components/segment.css'
    \./QuickView.jade : template
}

QuickView = Backbone.View.extend do
    el: $ \#quickView
    clear: !-> @$el.html ''

    # it: joint.dia.Element
    render: ->
        @clear!

        @$el.append template do
            model:
                data: it.model.attributes.properties
                text: 'Свойства и правила'

        @$el.append template do
            model:
                data: it.model.attributes.rules
                text: 'Правила перехода'

        @$el.append template do
            model:
                data: it.model.attributes.actions
                text: 'События и действия'

        @

    isNotEmpty: -> @$el.children!length

    events:
        'click': 'hidePopup'

    showPopup: !-> @$el.css \display , \block
    hidePopup: !-> @$el.css \display , \none

    # it: joint.dia.Element
    positionPopup: !->
        if it.targetPoint
        then
            targetPosition =
                x: Math.abs((it.targetPoint.x - it.sourcePoint.x) / 2 + it.sourcePoint.x)
                y: Math.abs((it.targetPoint.y - it.sourcePoint.y) / 2 + it.sourcePoint.y)

            @$el.css do
                top: targetPosition.y
                left: targetPosition.x
        else
            targetPosition = it.model.attributes.position
            targetSize = it.model.attributes.size

            @$el.css do
                top: targetPosition.y - targetSize.height/2
                left: targetPosition.x + targetSize.width


    # it: joint.dia.Element
    activatePopup: !->
        @positionPopup it
        if @isNotEmpty! then @showPopup!

export
    create: ->
        @element = new QuickView
        @

    # it: joint.dia.Element
    activatePopup: ->
        @element.render it
        @element.activatePopup it
        @
