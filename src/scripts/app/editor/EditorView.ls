require! {
    './EditorView.jade': template
}

PaperView = Marionette.ItemView.extend do
    el: $ \#container
    template: template

export
    create: ->
        @view = new PaperView
        @

    render: ->
        @view.render!
        @
