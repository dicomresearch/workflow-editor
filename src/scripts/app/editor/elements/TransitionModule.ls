require! {
    \observer.ls
}

observer.on do
    \transition:hover
    (element) !-> App.components.quickView.activatePopup element

observer.on do
    \transition:dblclick
    (element) !-> App.components.editWindow.activate element
