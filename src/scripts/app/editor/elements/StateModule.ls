require! {
    \observer.ls
}

observer.on do
    \state:hover
    (element) !-> App.components.quickView.activatePopup element

observer.on do
    \state:dblclick
    (element) !-> App.components.editWindow.activate element
