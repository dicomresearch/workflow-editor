require! {
    'observer.ls'
    'editor/EditorView.ls': editor
    'components/QuickView.ls': quick
    'components/EditWindow.ls': editWin
    'main/mainMenu/addItemWin/AddItemWin.ls'
    'components/EditItemWin.ls'
}

observer.on do
    \main:wfReady
    (wf) !->
        /*Создание и рендер компонента редактора*/
        App.views.editor = editor.create!
        App.views.editor.render!

        /*Создание и запуск движка визуализации*/
        App.engineInstance = Engine.create!
        App.engineInstance.generate wf

        /*Создание компонента быстрого просмотра*/
        App.components.quickView = quick.create!

        /*Создание компонента окна редактирования*/
        App.components.editWindow = editWin.create!

        /*Создание компонента окна добавления*/
        App.components.addWindow = AddItemWin.create!

        /*Создание компонента окна CRUD для списков*/
        App.components.editItemWindow = EditItemWin.create!
