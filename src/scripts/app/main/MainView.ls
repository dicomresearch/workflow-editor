require! {
    '../observer.ls'
    './MainView.jade': template
}

MainFormView = Marionette.ItemView.extend do
    el: $ \#container
    template: template

    events:
        'click #formWFButton': \showWF

    showWF: !->
        stringWF = $ \#textWF .val!
        objWF = $.parseJSON stringWF

        observer.trigger \main:wfReady , objWF

mainFormView = new MainFormView
mainFormView.render!
