require! {
    'semantic-ui/dist/semantic.css'
    'semantic-ui/dist/semantic.js'
    'main/mainMenu/addItemWin/addItemTabs/AddItemTabs.ls'
    'main/mainMenu/addItemWin/AddItewWin.jade': template
    'elements/state.ls'
    'elements/transition.ls'
}

Creator =
    createElement: (type, data) ->
        switch type
        | 'State' => new joint.shapes.wf.State data
        | 'Transition' => Engine.createRelation data, true
    addToPaper: (el) -> el |> Engine.Graph.addCell
    resetLayout: (engine)-> engine.createLayout!

    UCaddNewElement: (type, data) ->
        element = @createElement type, data
        element |> @addToPaper
        @resetLayout

AddItemWin = Marionette.View.extend do
    el: $ \#addWindow
    children: []

    show: ->
        @$el.modal \show
        @

    close: ->
        @$el.modal \hide
        @

    ui:
        saveBtn: '#saveAddWinData'
        close: '.close'

    events:
        'click @ui.saveBtn': 'saveData'
        'click @ui.close': 'close'

    saveData: ->
        newElement =
            transition: @options.transition

        _.forEach @children, (obj) ~>
            newData = obj.el.triggerMethod \save
            @deciderSave obj.name, newData, newElement

        switch @data.transition
        | true => Creator.UCaddNewElement 'Transition', newElement
        | false => Creator.UCaddNewElement 'State', newElement

        @close!

    saveMain: (target, data) -> _.merge target, data
    saveProps: (target, data) -> target.properties = data
    saveActions: (target, data) -> target.actions = data
    saveRules: (target, data) -> target.rules = data

    deciderSave: (type, data, target) ->
        mapper=
            main: @saveMain
            props: @saveProps
            actions: @saveActions
            rules: @saveRules

        mapper[type](target, data)

    # it: joint.dia.Element
    render: ->
        @data = it
        @$el.html template!

        tabs = new AddItemTabs do
            parent: @
            type: @data.type
            model: new Backbone.Model {transition: @data.transition}

        @$el.find \div.content .html tabs.render!el

        $ '.ui.tabular.menu .item' .tab!
        $ '.ui.accordion' .accordion!

        @

    activate: ->
        @show!
        @

export
    class: AddItemWin
    create: ->
        @element = new AddItemWin
        @

    # it: joint.dia.Element
    activate: ->
        @element.render it
        @element.activate!
        @
