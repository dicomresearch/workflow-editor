require! {
    'semantic-ui/dist/semantic.css'
    'semantic-ui/dist/semantic.js'
    './getWFWin.jade': template
    'zeroclipboard'
}

GetWFWin = Marionette.View.extend do
    el: $ \#getWFWin
    template: template

    render: ->
        @$el.html template @model

        @

    ui:
        copyBtn: '#copy'
        close: '.close'

    events:
        'click @ui.copyBtn': 'copy'
        'click @ui.close': 'close'

    show: ->
        @$el.modal \show
        @

    close: ->
        @$el.modal \hide
        @

module.exports = GetWFWin
