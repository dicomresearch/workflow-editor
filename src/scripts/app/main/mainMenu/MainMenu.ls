require! {
    'semantic-ui/dist/semantic.css'
    'semantic-ui/dist/semantic.js'
    'main/mainMenu/getWFWin/getWFWin.ls': GetWFWin
    'main/mainMenu/addItemWin/AddItemWin.ls'
    './MainMenu.jade': template
}

MainMenu = Marionette.ItemView.extend do
    el:  $ \#mainMenu
    template: template

    onRender: !-> @$el.find '.ui.dropdown' .dropdown!

    ui:
        getWFBtn: '#getWFBtn'
        addStateBtn: '#addStateBtn'
        addTransitionBtn: '#addTransitionBtn'

    events:
        'click @ui.getWFBtn': 'openGetWFWin'
        'click @ui.addStateBtn': 'openCreateStateWin'
        'click @ui.addTransitionBtn': 'openCreateTransitionWin'

    openGetWFWin: !->
        currentWF = Engine.getCurrentWF!
        currentWFString = currentWF |> JSON.stringify

        getWFWin = new GetWFWin {model: wf: currentWFString}

        getWFWin.render!
        getWFWin.show!

    openCreateStateWin: !->
        addItemWin = App.components.addWindow.activate do
            model: @
            type: 'wf.State'
            transition: false

    openCreateTransitionWin: !->
        addItemWin = App.components.addWindow.activate do
            model: @
            type: 'wf.Transition'
            transition: true

mainMenu = new MainMenu
mainMenu.render!
