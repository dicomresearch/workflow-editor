var webpack = require('webpack');

module.exports = function (config) {
    config.set({
        browsers: [ 'Chrome' ], //run in Chrome
        singleRun: true, //just run once by default
        frameworks: [ 'mocha' ], //use the mocha test framework
        files: [
            'tests.webpack.js' //just load this file
        ],
        preprocessors: {
            'tests.webpack.js': [ 'webpack' ] //preprocess with webpack and our sourcemap loader
        },
        reporters: [ 'mocha' ], //report results in this format
        webpack: { //kind of a copy of your webpack config
            module: {
                loaders: [
                    { test: /\.ls$/, loader: 'livescript-loader' },
                    { test: /\.json$/, loader: 'json-loader' }
                ]
            },
            plugins: [
                new webpack.ProvidePlugin({
                    $: 'jquery',
                    joint: 'jointjs',
                    _: 'lodash',
                    Backbone: 'backbone',
                    chai: 'chai'
                })
            ],
            resolve: {
                modulesDirectories: ['app', 'node_modules', 'engine', 'utils'],
                alias: {
                    'engine': __dirname + '/src/scripts/engine/engine.ls',
                    'transitionElement': __dirname + '/src/scripts/engine/elements/transition.ls',
                    'stateElement': __dirname + '/src/scripts/engine/elements/state.ls',
                    'converters':  __dirname + '/src/scripts/utils/converters.ls',
                    'mockWF': __dirname + '/wf-example.json'
                }
            },
        },
        webpackMiddleware: {
            stats: {
                colors: true
            }
        },

        plugins: [
            require('karma-mocha'),
            require('karma-chrome-launcher'),
            require('karma-webpack'),
            require('karma-mocha-reporter')
        ]
    });
};
